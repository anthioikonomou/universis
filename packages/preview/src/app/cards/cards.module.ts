import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CardsRoutingModule} from './cards.routing';
import {HighlightIncludeModule} from '../highlight/highlight.module';
import {CardsComponent} from './components/cards-component/cards.component';
import {ExpandableCardsComponent} from './components/expandable-cards/expandable-cards.component';
import {ProfileCardComponent} from './components/profile-card/profile-card.component';

@NgModule({
  imports: [
    CardsRoutingModule,
    HighlightIncludeModule
  ],
  declarations: [
    CardsComponent,
    ExpandableCardsComponent,
    ProfileCardComponent
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ]
})
export class CardsModule {}
