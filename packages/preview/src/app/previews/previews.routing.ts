import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {GraduationRulesComponent} from './components/graduation-rules/graduation-rules.component';
import {GraduationProgressComponent} from './components/graduation-progress/graduation-progress.component';
import { GraduationSimplifiedComponent } from './components/graduation-simplified/graduation-simplified.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'graduation-rules'
  },
  {
    path: 'graduation-rules',
    component: GraduationRulesComponent,
    data: {
      title: 'Student Graduation Rules'
    }
  },
  {
    path: 'graduation-progress',
    component: GraduationProgressComponent,
    data: {
      title: 'Student Graduation Progress'
    },
  },
  {
    path: 'graduation-simplified',
    component: GraduationSimplifiedComponent,
    data: {
      title: 'Simplified Student Graduation Rules'
    }
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PreviewsRoutingModule {
  constructor() {}
}
