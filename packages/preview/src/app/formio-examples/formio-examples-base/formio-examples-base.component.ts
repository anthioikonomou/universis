import { Component, OnInit } from '@angular/core';
// import { FormioComponent } from 'angular-formio/components/formio/formio.component';

@Component({
  selector: 'app-formio-examples-base',
  templateUrl: './formio-examples-base.component.html',
  styleUrls: ['./formio-examples-base.component.scss']
})
export class FormioExamplesBaseComponent implements OnInit {

  formioTextfieldConfig: any = {};
  formioEmailConfig: any = {};
  formioNumberConfig: any = {};
  formioPhoneNumberConfig: any = {};
  formioDateConfig: any = {};
  formioRadioConfig: any = {};
  formioCheckboxConfig: any = {};
  formioSelectHtml5Config: any = {};
  formioSelectChoicesJSConfig: any = {};
  formioTooltipConfig: any = {};
  formioDescriptionConfig: any = {};
  formioLayoutColumnsConfig: any = {};
  formioLayoutFieldsetConfig: any = {};
  formioLayoutTabConfig: any = {};
  formioValidationSimpleConfig: any = {};
  formioValidationNumberConfig:any = {};
  formioValidationMinMaxNumberConfig: any = {};
  formioValidationMaskNumberConfig: any = {};
  formioValidationRegExpConfig: any = {};
  formioValidationCustomConfig: any = {};
  formioValidateDisabledInput: any = {};
  formioStyleCustomClassConfig: any = {};
  
  constructor() { }

  ngOnInit() {

    this.formioTextfieldConfig['object']  = {
      "components": [
        {
          "label": "Textfield Label",
          "labelPosition": "top",
          "placeholder": "This is a textfield",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }
    this.formioTextfieldConfig['txt'] = JSON.stringify(this.formioTextfieldConfig['object'], null, "  ");

    this.formioEmailConfig['object']  = {
      "components": [
        {
          "label": "Email Label",
          "labelPosition": "top",
          "placeholder": "This is an email field",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "email",
          "input": true
        }
      ]
    }
    this.formioEmailConfig['txt'] = JSON.stringify(this.formioEmailConfig['object'], null, "  ");

    this.formioNumberConfig['object']  = {
      "components": [
        {
          "label": "Number Label",
          "labelPosition": "top",
          "placeholder": "This is a number field (only numbers allowed)",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }
    this.formioNumberConfig['txt'] = JSON.stringify(this.formioNumberConfig['object'], null, "  ");

    this.formioPhoneNumberConfig['object']  = {
      "components": [
        {
          "label": "Phone Number Label",
          "labelPosition": "top",
          "inputMask": "+999999999999",
          "placeholder": "This is a phone number field (+302310123456)",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "phoneNumber",
          "input": true
        }
      ]
    }
    this.formioPhoneNumberConfig['txt'] = JSON.stringify(this.formioPhoneNumberConfig['object'], null, "  ");

    this.formioDateConfig['object']  = {
      "components": [
        {
          "label": "Date Label",
          "format": "yyyy-MM-dd",
          "enableTime": false,
          "labelPosition": "top",
          "placeholder": "This is a date field",
          "suffix": "<i ref=\"icon\" class=\"fa fa-calendar\" style=\"\"></i>",
          "widget": {
            "type": "calendar",
            "format": "yyyy-MM-dd"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "datetime",
          "input": true
        }
      ]
    }
    this.formioDateConfig['txt'] = JSON.stringify(this.formioDateConfig['object'], null, "  ");

    this.formioRadioConfig['object']  = {
      "components": [
        {
          "label": "Radio buttons",
          "labelPosition": "top",
          "inline": true,
          "values": [
            {
              "label": "1",
              "value": "1"
            },
            {
              "label": "2",
              "value": "2"
            },
            {
              "label": "3",
              "value": "3"
            }
          ],
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "radio",
          "input": true
        }
      ]
    }
    this.formioRadioConfig['txt'] = JSON.stringify(this.formioRadioConfig['object'], null, "  ");

    this.formioCheckboxConfig['object'] = {
      "components": [
        {
          "label": "Checkbox",
          "defaultValue": false,
          "key": "checkbox",
          "type": "checkbox",
          "input": true
        }
      ]
    }
    this.formioCheckboxConfig['txt'] = JSON.stringify(this.formioCheckboxConfig['object'], null, "  ");
    
    this.formioSelectHtml5Config['object'] = {
      "components": [
        {
          "label": "Dropdown list",
          "labelPosition": "top",
          "dataSrc": "url",
          "data": {
              "url": "APIendpoint",
              "headers": []
          },
          "template": "<span class='col-4 text-truncate'>{{ item.alternateName }}</span>",
          "selectValues": "value",
          "widget": "html5",
          "validate": {
              "required": false
          },
          "key": "model-property-name",
          "type": "select",
          "input": true,
        }
      ]
    }
    this.formioSelectHtml5Config['txt'] = JSON.stringify(this.formioSelectHtml5Config['object'], null, "  ");
    
    this.formioSelectChoicesJSConfig['object'] = {
      "components": [
        {
          "label": "Dropdown list",
          "labelPosition": "top",
          "dataSrc": "url",
          "data": {
              "url": "APIendpoint",
              "headers": []
          },
          "widget": "choicesjs",
          "template": "<span class='col-4 text-truncate'>{{ item.alternateName }}</span>",
          "selectValues": "value",
          "valueProperty": "",
          "defaultValue": "",
          "lazyLoad": false,
          "searchFields": [],
          "search": "$search",
          "validate": {
              "required": false
          },
          "key": "model-property-name",
          "type": "select",
          "input": true,
        }
      ]
    }
    this.formioSelectChoicesJSConfig['txt'] = JSON.stringify(this.formioSelectChoicesJSConfig['object'], null, "  ");
    
    this.formioTooltipConfig['object'] = {
      "components": [
        {
          "label": "Informational tooltip for an element",
          "tooltip": "Check this if you need to check it",
          "defaultValue": false,
          "key": "checkbox",
          "type": "checkbox",
          "input": true
        }
      ]
    }
    this.formioTooltipConfig['txt'] = JSON.stringify(this.formioTooltipConfig['object'], null, "  ");
    
    this.formioDescriptionConfig['object'] = {
      "components": [
        {
          "label": "Description for an element",
          "labelPosition": "top",
          "placeholder": "This is a textfield",
          "description" : "This is the decription for the field above",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }
    this.formioDescriptionConfig['txt'] = JSON.stringify(this.formioDescriptionConfig['object'], null, "  ");
    
    this.formioLayoutColumnsConfig['object'] = {
      "components": [
        {
          "label": "Columns",
          "columns": [
            {
              "components": [
                {
                  "label": "Column 1 Component",
                  "labelPosition": "top",
                  "placeholder": "Simple textfield",
                  "key": "model-property-name",
                  "type": "textfield"
                }
              ],
              "width": 4
            },
            {
              "components": [
                {
                  "label": "Column 2 Component",
                  "labelPosition": "top",
                  "placeholder": "Simple textfield",
                  "key": "model-property-name",
                  "type": "textfield"
                }
              ],
              "width": 4
            },
            {
              "components": [
                {
                  "label": "Column 3 Component",
                  "labelPosition": "top",
                  "placeholder": "Simple textfield",
                  "key": "model-property-name",
                  "type": "textfield"
                }
              ],
              "width": 4
            }
          ],
          "key": "columns",
          "type": "columns",
          "input": false
        }
      ]
    }
    this.formioLayoutColumnsConfig['txt'] = JSON.stringify(this.formioLayoutColumnsConfig['object'], null, "  ");
    
    this.formioLayoutFieldsetConfig['object'] = {
      "components": [
        {
          "legend": "Group 1",
          "type": "fieldset",
          "components": [
            {
              "label": "Fieldset 1 Component",
              "labelPosition": "top",
              "placeholder": "Simple textfield",
              "key": "model-property-name",
              "type": "textfield"
            }
          ]
        },
        {
          "legend": "Group 2",
          "type": "fieldset",
          "components": [
            {
              "label": "Fieldset 1 Component",
              "labelPosition": "top",
              "placeholder": "Simple textfield",
              "key": "model-property-name",
              "type": "textfield"
            }
          ]
        },
      ]
    }
    this.formioLayoutFieldsetConfig['txt'] = JSON.stringify(this.formioLayoutFieldsetConfig['object'], null, "  ");
    
    this.formioLayoutTabConfig['object'] = {
      "components": [
        {
          "label": "Tabs",
          "type": "tabs",
          "key": "tabs1",
          "path": "tabs1",
          "input": false,
          "components": [
            {
              "label": "Tab Section 1",
              "components": [
                {
                  "label": "Tab 1 Component",
                  "labelPosition": "top",
                  "placeholder": "Simple textfield",
                  "key": "model-property-name",
                  "type": "textfield"
                }
              ]
            },
            {
              "label": "Tab Section 2",
              "components": [
                {
                  "label": "Tab 2 Component",
                  "labelPosition": "top",
                  "placeholder": "Simple textfield",
                  "key": "model-property-name",
                  "type": "textfield"
                }
              ]
            }
          ]
        }
      ]
    }
    this.formioLayoutTabConfig['txt'] = JSON.stringify(this.formioLayoutTabConfig['object'], null, "  ");
    
    this.formioValidationSimpleConfig['object'] = {
      "components": [
        {
          "label": "A Textfield Label",
          "labelPosition": "top",
          "placeholder": "You should insert something in here",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }
    this.formioValidationSimpleConfig['txt'] = JSON.stringify(this.formioValidationSimpleConfig['object'], null, "  ");
    
    this.formioValidationNumberConfig['object'] = {
      "components": [
        {
          "label": "A Number Label",
          "labelPosition": "top",
          "placeholder": "If type is set to number it automatically expects a number to be entered",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }
    this.formioValidationNumberConfig['txt'] = JSON.stringify(this.formioValidationNumberConfig['object'], null, "  ");
    
    this.formioValidationMinMaxNumberConfig['object'] = {
      "components": [
        {
          "label": "A Number Label",
          "labelPosition": "top",
          "placeholder": "You are expected to insert an integer between 1 and 5",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true,
            "integer": true,
            "min": 1,
            "max": 5
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }
    this.formioValidationMinMaxNumberConfig['txt'] = JSON.stringify(this.formioValidationMinMaxNumberConfig['object'], null, "  ");
    
    this.formioValidationMaskNumberConfig['object'] = {
      "components": [
        {
          "label": "A masked text field",
          "labelPosition": "top",
          "placeholder": "You are expected to insert a 4 digit number",
          "inputMask": "9999",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }
    this.formioValidationMaskNumberConfig['txt'] = JSON.stringify(this.formioValidationMaskNumberConfig['object'], null, "  ");
    
    this.formioValidationRegExpConfig['object'] = {
      "components": [
        {
          "label": "Validate using a regular expression",
          "labelPosition": "top",
          "placeholder": "Expected input is in the form 11-AAA-1111.AA",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true,
            "pattern": "[0-9]{2}-[A-Z]{3}-[0-9]{4}\.[A-Z]{2}"
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }
    this.formioValidationRegExpConfig['txt'] = JSON.stringify(this.formioValidationRegExpConfig['object'], null, "  ");
    
    this.formioValidationCustomConfig['object'] = {
      "components": [
        {
          "label": "Custom Validation and error message",
          "labelPosition": "top",
          "placeholder": "Expected a 10 digit integer as a number",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": true,
            "integer": true,
            "custom": "valid = (/^[0-9]{10}$/.test(input)) ? true : 'Please enter a 10 digit number';"
          },
          "key": "model-property-name",
          "type": "number",
          "input": true
        }
      ]
    }
    this.formioValidationCustomConfig['txt'] = JSON.stringify(this.formioValidationCustomConfig['object'], null, "  ");
    
    this.formioValidateDisabledInput['object'] = {
      "components": [
        {
          "label": "Textfield disabled for input",
          "labelPosition": "top",
          "disabled": true,
          "placeholder": "This is a textfield",
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": false
        }
      ]
    }
    this.formioValidateDisabledInput['txt'] = JSON.stringify(this.formioValidateDisabledInput['object'], null, "  ");
    
    this.formioStyleCustomClassConfig['object'] = {
      "components": [
        {
          "label": "CSS styling using custom class",
          "labelPosition": "top",
          "customClass": "text-success",
          "placeholder": "This is a textfield",
          "widget": {
            "type": "input"
          },
          "validate": {
            "required": false
          },
          "key": "model-property-name",
          "type": "textfield",
          "input": true
        }
      ]
    }
    this.formioStyleCustomClassConfig['txt'] = JSON.stringify(this.formioStyleCustomClassConfig['object'], null, "  ");

  }
}


