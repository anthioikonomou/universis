import { Component, Input, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-requests-thesis-action',
  templateUrl: './requests-thesis-action.component.html'
})
export class RequestsThesisActionComponent implements OnInit {
  @Input('request') request;
  public thesisProposal: any;
  constructor(private _context: AngularDataContext) { }

  async ngOnInit() {
    const thesisProposalID = (await this._context.model('ThesisRequestActions')
                                              .where('id')
                                              .equal(this.request.id)
                                              .select('thesisProposal as result')
                                              .expand('thesisProposal')
                                              .getItem()).result;
    this.thesisProposal = await this._context.model('ThesisProposals')
                                              .where('id')
                                              .equal(thesisProposalID)
                                              .expand('department,instructor,status,type')
                                              .getItem();
        
  }

}
