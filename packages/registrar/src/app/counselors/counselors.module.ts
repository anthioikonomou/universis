import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CounselorsHomeComponent} from './components/counselors-home/counselors-home.component';
import {CounselorsTableComponent} from './components/counselors-table/counselors-table.component';
import {CounselorsRootComponent} from './components/counselors-root/counselors-root.component';
import {TablesModule} from '@universis/ngx-tables';
import {TranslateModule} from '@ngx-translate/core';
import {CounselorsRoutingModule} from './counselors.routing';
import {CounselorsSharedModule} from './counselors.shared';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {ElementsModule} from '../elements/elements.module';
import {MostModule} from '@themost/angular';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import {RouterModalModule} from '@universis/common/routing';
import {BsDatepickerModule, TooltipModule} from 'ngx-bootstrap';
import {DatePipe} from '@angular/common';
import {AdvancedFormsModule} from '@universis/forms';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    CounselorsSharedModule,
    TablesModule,
    CounselorsRoutingModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    MostModule,
    RegistrarSharedModule,
    RouterModalModule,
    AdvancedFormsModule,
    BsDatepickerModule.forRoot(),
    TooltipModule.forRoot(),
  ],
  providers: [DatePipe],
  declarations: [
    CounselorsHomeComponent,
    CounselorsRootComponent,
    CounselorsTableComponent

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CounselorsModule {
}
