import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesOverviewClassesComponent } from './courses-overview-classes.component';

describe('CoursesOverviewClassesComponent', () => {
  let component: CoursesOverviewClassesComponent;
  let fixture: ComponentFixture<CoursesOverviewClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesOverviewClassesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesOverviewClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
