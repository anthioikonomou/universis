import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentsRulesComponent } from './students-rules.component';

describe('StudentsRulesComponent', () => {
  let component: StudentsRulesComponent;
  let fixture: ComponentFixture<StudentsRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentsRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentsRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
