import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import { AppEventService, DIALOG_BUTTONS, ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-students-overview-suspensions',
  templateUrl: './students-overview-suspensions.component.html',
  styles: [`
    .suspension-row:last-child {
      border-bottom: 0 !important;
    }
    .delete-link {
      cursor: pointer
    }
    .delete-link:hover {
      text-decoration: underline !important;
    }
  `]
})
export class StudentsOverviewSuspensionsComponent implements OnInit, OnDestroy  {

  public lastSuspensions: any;
  @Input() studentId: number;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  private reloadSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _appEventService: AppEventService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      // load suspensions
      await this.loadSuspensions();
    });
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(async (fragment) => {
      if (fragment && fragment === 'reload') {
        // load suspensions
        await this.loadSuspensions();
        // fire actions reload event
        this._appEventService.change.next({
          model: 'RefreshStudentActions',
          target: this.studentId
        });
      }
    });

    this.reloadSubscription = this._appEventService.changed.subscribe(async (change) => {
      if (change && change.model === 'UpdateStudentSuspensions' && change.target === this.studentId) {
        // load suspensions
        await this.loadSuspensions();
      }
    });
  }

  sortByIdentifier (items) {
    items.forEach(el => el.virtualColumn = el['identifier'].replace(/_/g, ''));
    items.sort(function (a, b) {
      return b.virtualColumn - a.virtualColumn;
    });
    return items;
  }

  async loadSuspensions() {
    this.lastSuspensions = await this._context.model('StudentSuspensions')
      .where('student').equal(this.studentId)
      .expand('student($expand=studentStatus)')
      .orderByDescending('identifier')
      .getItems();
    // sort by type string 'identifier'
    this.lastSuspensions = this.sortByIdentifier(this.lastSuspensions);
    if (this.lastSuspensions && this.lastSuspensions.length) {
      // get latest suspension
      const lastSuspension = this.lastSuspensions[0];
      // append isDeletable property
      this.inferIsDeletable(lastSuspension);
    }
  }

  inferIsDeletable(suspension: {reintegrated: boolean | number, student: {studentStatus: {alternateName: string}}}) {
    if (suspension == null) {
      return;
    }
    // latest suspension is deletable
    // only if student is not reintegrated and is suspended
    Object.defineProperty(suspension, 'isDeletable', {
      configurable: false,
      writable: false,
      enumerable: true,
      value:
        !suspension.reintegrated &&
        suspension.student &&
        suspension.student.studentStatus &&
        suspension.student.studentStatus.alternateName === 'suspended',
    });
  }

  async deleteStudentSuspension(suspension: {id: string, isDeletable: boolean}) {
    try {
      // validate suspension
      if (!(suspension && suspension.isDeletable)) {
        return Promise.resolve();
      }
      // fetch action translations
      const actionTranslations: {
        Description: string;
        ModalMessage: string;
        ToastMessage: string;
      } = this._translateService.instant('Students.DeleteSuspensionAction');
      // get user confirmation for deletion
      const dialogResult = await this._modalService.showWarningDialog(actionTranslations.Description,
        actionTranslations.ModalMessage,
        DIALOG_BUTTONS.OkCancel);
      if (dialogResult !== 'ok') {
        return Promise.resolve();
      }
      // show loading
      this._loadingService.showLoading();
      // execute deletion
      await this._context.getService().execute({
        method: 'POST',
        url: `StudentSuspensions/${suspension.id}/delete`,
        headers: {},
        data: null
      });
      // hide loading
      this._loadingService.hideLoading();
      // show success toast message
      this._toastService.show(actionTranslations.Description, actionTranslations.ToastMessage);
      // and fire reload events
      // first local reload event
      this._appEventService.change.next({
        model: 'UpdateStudentSuspensions',
        target: this.studentId
      });
      // and finally actions/preview reload event
      this._appEventService.change.next({
        model: 'UpdateStudentStatusActions'
      });
      return;
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      // ensure loading is hidden
      this._loadingService.hideLoading();
    }
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }
}
