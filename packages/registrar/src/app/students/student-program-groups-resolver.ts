import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {AngularDataContext} from '@themost/angular';

export class StudentProgramGroupsResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext) {
  }
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return Promise.all([this._context.model('StudentProgramGroups').where('student').equal(route.params.id)
      .expand('programGroup').getItems(),
      this._context.model('Students').where('id').equal(parseInt(route.params.id)).getItem()]).then(res => {
      return {
        currentProgram: res[1].studyProgram,
        object: res[1].id
      }
    }).catch(err => {
      console.log(err);
      return {};
    });
  }
}
