import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FieldGroupOverviewComponent } from './components/field-group-overview/field-group-overview.component';
import { AdvancedListComponent, AdvancedSearchConfigurationResolver, AdvancedTableConfigurationResolver } from '@universis/ngx-tables';
import { AdvancedFormItemWithLocalesResolver, AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormResolver } from '@universis/forms';
// todo: move this resolver in a shared library and avoid using an internal dependency
import { ItemLocalesArrayResolver } from '../registrar-shared/services/activeDepartmentService.service';
import { ParentFieldGroupResolver, ReadonlyAccessResolver } from './resolvers';

const routes: Routes = [
  {
    path: 'configuration/groups/:id/overview',
    component: FieldGroupOverviewComponent,
    children: [
      {
        path: 'edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'ConsentFieldGroups',
          modalOptions: {
            modalTitle: 'Settings.EditItem'
          },
          action: 'edit',
          serviceQueryParams: {
            $expand: 'locales',
          },
          closeOnSubmit: true,
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          data: AdvancedFormItemWithLocalesResolver
        }
      },
      {
        path: 'fields/add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'ConsentFields',
          modalOptions: {
            modalTitle: 'Settings.NewItem'
          },
          action: 'edit',
          closeOnSubmit: true,
          description: null
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          locales: ItemLocalesArrayResolver,
          group: ParentFieldGroupResolver
        }
      },
      {
        path: 'fields/:id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'ConsentFields',
          modalOptions: {
            modalTitle: 'Settings.EditItem'
          },
          action: 'edit',
          serviceQueryParams: {
            $expand: 'locales,group',
          },
          closeOnSubmit: true,
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          data: AdvancedFormItemWithLocalesResolver
        }
      }
    ]
  },
  {
    path: 'configuration/groups',
    component: AdvancedListComponent,
    data: {
      model: 'ConsentFieldGroups',
      description: 'Settings.Lists.ConsentFieldGroups.Description',
      longDescription: 'Settings.Lists.ConsentFieldGroups.LongDescription',
      list: 'index'
    },
    resolve: {
      tableConfiguration: AdvancedTableConfigurationResolver,
      readonly: ReadonlyAccessResolver
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          model: 'ConsentFieldGroups',
          modalOptions: {
            modalTitle: 'Settings.NewItem'
          },
          action: 'edit',
          closeOnSubmit: true,
          description: null
        },
        resolve: {
          formConfig: AdvancedFormResolver,
          locales: ItemLocalesArrayResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    ReadonlyAccessResolver
  ]
})
export class UserConsentAdminRoutingModule { }
