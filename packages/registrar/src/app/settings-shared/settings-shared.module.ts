import { NgModule, OnInit, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MostModule } from '@themost/angular';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { environment } from '../../environments/environment';
import { SettingsService } from './services/settings.service';

@NgModule({
  imports: [
    CommonModule,
    MostModule,
    TranslateModule
  ],
  declarations: [
  ]
})
export class SettingsSharedModule implements OnInit {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SettingsSharedModule,
            providers: [
              SettingsService
            ]
        };
        }
  constructor(@Optional() @SkipSelf() parentModule: SettingsSharedModule,
  private _translateService: TranslateService) {
      this.ngOnInit().catch( err => {
        console.error('An error occurred while loading SettingsSharedModule');
        console.error(err);
      });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
        import(`./i18n/settings.${language}.json`).then((translations) => {
          this._translateService.setTranslation(language, translations, true);
        });
      });
  }
}
