export const APP_LOCATIONS_FEATURES = [
    {
      'help': 'Enables student internship feature',
      'target': {
          'url': '^/internships'
      }
    },
    {
      'help': 'Enables student thesis feature',
      'target': {
          'url': '^/internships'
      }
    },
    {
      'help': 'Enables student scholarship feature',
      'target': {
          'url': '^/scholarships'
      }
    },
    {
      'help': 'Enables student counselors feature',
      'target': {
          'url': '^/students/counselors'
      }
    },
    {
        'help': 'Enables student semester feature',
        'target': {
            'url': '^/students/semester'
        }
      },
      {
        'help': 'Enables student courses catalogue feature',
        'target': {
            'url': '^/students/courses'
        }
      },
    {
      'help': 'Enables student period registration features',
      'target': {
          'url': '^/registrations'
      }
    },
    {
      'help': 'Enables study program specialization features like manage specialization, view specialization courses etc',
      'target': {
          'url': '^/study-programs/specializations'
      }
    },
    {
      'help': 'Enables study program courses group features like manage group of courses, assign group of course to students etc',
      'target': {
          'url': '^/study-programs/groups'
      }
    },
    {
      'help': 'Enables study program semester rules',
      'target': {
          'url': '^/study-programs/semesterRules'
      }
    }
  ]
  