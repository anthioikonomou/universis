import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { EnrollmentEventViewersComponent } from './event-overview-viewers/event-overview-viewers.component';

@Component({
  selector: 'app-enrollment-event-overview',
  templateUrl: './event-overview.component.html'
})
export class EnrollmentEventOverviewComponent implements OnInit {

  public enrollmentEvent: any;
  @ViewChild(EnrollmentEventViewersComponent) viewersList: EnrollmentEventViewersComponent;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  async ngOnInit() {

    const enrollmentEvent = await this._context.model('StudyProgramEnrollmentEvents')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('studyProgram,attachmentTypes($expand=attachmentType),articles($expand=actionStatus;$orderby=actionStatus,inLanguage),inscriptionModes')
      .getItem();
    this.enrollmentEvent = enrollmentEvent;
  }

  /**
   * Refreshes the viewers list by reinitializing the component.
   * @remarks
   * This method is called when the user adds a viewer to the list.
   */
  refreshViewers() {
    // Refresh the viewers list here
    this.viewersList.ngOnInit();
  }
}
