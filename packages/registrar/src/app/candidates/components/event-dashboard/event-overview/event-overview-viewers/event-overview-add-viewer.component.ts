import { Component, Input } from '@angular/core';
// tslint:disable-next-line:max-line-length
import { AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate } from '@universis/ngx-tables';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import { AdvancedFilterValueProvider } from '@universis/ngx-tables';
import { DatePipe } from '@angular/common';
import { EnrollmentEventOverviewComponent } from '../event-overview.component';

@Component({
  selector: 'app-event-overview-add-viewer',
  template: `<div><label [translate]="'Candidates.Viewers.SelectList'"></label></div>` + AdvancedTableModalBaseTemplate
})
export class EventOverviewAddViewerComponent extends AdvancedTableModalBaseComponent {

  @Input() enrollmentEvent: any;
  public model: any = {};
  public viewers: any = {};

  constructor(_router: Router, _activatedRoute: ActivatedRoute,
    _context: AngularDataContext,
    private _errorService: ErrorService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
    protected advancedFilterValueProvider: AdvancedFilterValueProvider,
    protected datePipe: DatePipe,
    private parent: EnrollmentEventOverviewComponent
  ) {
    super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    // set default title
    this.modalTitle = 'Candidates.Viewers.Add';
    this.tableConfigSrc = 'assets/tables/Users/config.all.json'
  }

  hasInputs(): Array<string> {
    return ['enrollmentEvent'];
  }

  /**
   * Sync selected viewers before submit
   */
  private async beforeSubmitViewers() {

    const selectedItems = this.advancedTable.selected;
    this.model = this.enrollmentEvent;
    this.model.viewers = this.model.viewers || [];
    // try to remove items (set item state to be removed)
    this.model.viewers.forEach((viewer) => {
      const selectedIndex = selectedItems.findIndex((x) => x.id === viewer.id);
      if (selectedIndex < 0) {
        // set state to delete
        Object.defineProperty(viewer, '$state', {
          configurable: true,
          writable: true,
          enumerable: true,
          value: 4
        });
      }
    });
    selectedItems.forEach((viewer) => {
      const findIndex = this.model.viewers.findIndex((x) => x.id === viewer.id);
      if (findIndex < 0) {
        // add item
        this.model.viewers.push(viewer);
      }
    });
  }


  async ok(): Promise<any> {

    // get selected items
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      // try to add classes
      items = selected.map(viewers => {
        return {
          viewers: viewers.id
        };
      });

      this.beforeSubmitViewers()
      this.model.id = this.enrollmentEvent.id;
      return await this._context.model('StudyProgramEnrollmentEvents')
        .save(this.model)
        .then(result => {
          // add toast message
          this._toastService.show(
            this._translateService.instant('Candidates.Viewers.AddViewersMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'Candidates.Viewers.AddViewersMessage.one' : 'Candidates.Viewers.AddViewersMessage.many')
              , { value: items.length })
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch(err => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
  }

  ngOnDestroy() {
    this.parent.refreshViewers();
  }
}
