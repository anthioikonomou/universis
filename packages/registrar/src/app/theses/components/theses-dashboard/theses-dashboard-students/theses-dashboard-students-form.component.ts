import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-theses-dashboard-students-form',
  templateUrl: './theses-dashboard-students-form.component.html'
})
export class ThesesDashboardStudentsFormComponent implements OnInit {

  @Input() student: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  }

}
