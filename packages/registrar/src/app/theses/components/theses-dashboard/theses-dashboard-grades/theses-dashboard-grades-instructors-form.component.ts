import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-theses-dashboard-grades-instructors-form',
  templateUrl: './theses-dashboard-grades-instructors-form.component.html'
})
export class ThesesDashboardGradesInstructorsFormComponent implements OnInit {

  @Input() thesisResults: any;
  @Input() editMode: boolean;
  @Input() editableGrades: boolean;
  @Input() gsIsNumeric: boolean;
  @Input() gradeScaleValues: any[];
  @Input() source: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService) {
  }

  async ngOnInit() {
  }

}
