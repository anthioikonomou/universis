import { AfterViewInit, Component, Input, OnChanges, OnDestroy, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import { ErrorService, LoadingService } from '@universis/common';
import { AdvancedFormsService, ServiceUrlPreProcessor } from '@universis/forms';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { cloneDeep } from 'lodash';
import { replaceClassesForm } from './replaceClasses.form';

@Component({
  selector: 'one-roster-replace-classes',
  template: `
  <!--pre-loading state-->
  <ng-template #loading>
    <div class="card shadow-none mb-0 mt-2">
        <div class="card-body">
          <div class="ph-col-12">
            <div class="ph-row">
              <div class="ph-col-8 big"></div>
            </div>
            <div class="ph-row">
              <div class="ph-col-8 big"></div>
            </div>
            <div class="ph-row">
              <div class="ph-col-2 big"></div>
              <div class="ml-4 ph-col-4 big"></div>
            </div>
            <div class="ph-row">
              <div class="ph-col-4 big"></div>
            </div>
          </div>
        </div>
    </div>
  </ng-template>
    <!--end pre-loading state-->
  <div class="card mb-0 shadow-none">
          <div class="card-body py-2">
            <div class="d-flex">
              <div>
                <h3 id="ReplaceClasses" class="card-title mb-1 font-weight-normal" [translate]="'OneRoster.ReplaceClasses'"></h3>
                <p class="card-text" innerHTML="{{ 'OneRoster.ReplaceClassesDescription' | translate }}">
        </p>
              </div>
            </div>
            <div>
              <ng-container *ngIf="(replacedBy | async) as replacedByClass">
                <ng-container *ngIf="replacedByClass && replacedByClass.value != null">
                    <h5 class="my-3 text-success font-weight-normal" [translate]="'OneRoster.CannotReplaceClassMessage'"></h5>
                </ng-container>
                <div class="mt-3">
                  <ng-container *ngIf="isLoading">
                      <ng-container *ngTemplateOutlet="loading"></ng-container>
                  </ng-container>
                    <ng-container>
                        <!-- load form in the background -->
                        <div [ngClass]="{ 'd-none': isLoading || (replacedByClass && replacedByClass.value != null) }">
                          <formio (formLoad)="onFormLoad()" (customEvent)="onEvent($event)" [renderOptions]="renderOptions" [form]="form" #formComponent [submission]="{ data: data}" ></formio>
                        </div>
                    </ng-container>
                </div>
              </ng-container>
          </div>
        </div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./one-roster-class-configuration.component.scss']
})
export class OneRosterReplaceClassesComponent implements OnChanges, AfterViewInit, OnDestroy {

  public form: any;
  public data: any = {};
  private previous: any = {};
  public renderOptions: {
    language: string;
    i18n: any;
  };

  @Input('courseClass') courseClass: { id: string, title: string, displayCode: string, year: string, period: string, departmentName: string, sections: any[] };

  @Input('replacedBy') replacedBy: BehaviorSubject<{ value: any }>;

  public replaces$ = new BehaviorSubject<any>([]);

  private get replaces$$(): Observable<any[]> {
    return from(
      this.context.model('OneRosterReplaceClasses').where('replacedBy').equal(this.courseClass.id)
      .expand('courseClass($select=id,title,course/displayCode as displayCode,course/department/name as departmentName,year/name as yearName,period/name as periodName)').getItems()
    )
  }

  subscription: any;
  isLoading: boolean = true;

  constructor(
    private context: AngularDataContext,
    private activatedRoute: ActivatedRoute, 
    private errorService: ErrorService,
    private loadingService: LoadingService,
    private formService: AdvancedFormsService,
    private _translateService: TranslateService
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.courseClass && changes.courseClass.currentValue) {
      this.replaces$$.subscribe((items) => {
        // load form
        const form = cloneDeep(replaceClassesForm);
        new ServiceUrlPreProcessor(this.context).parse(form);
          const { currentLang: language } = this._translateService;
          this.renderOptions = {
            language,
            i18n: {
              [language]: form.settings.i18n[language]
            }
          }
          this.data = {
            courseClass: changes.courseClass.currentValue,
            courseClasses: items.map((x) => x.courseClass)
          }
          this.form = form;
          this.replaces$.next(items);
      })
    }
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.replacedBy) {
      this.replacedBy.unsubscribe();
    }
  }
  ngAfterViewInit(): void {
    
  }

  onEvent(event: any) {
    if (event.type === 'replace') {
      this.loadingService.showLoading();
      const previous = this.data.courseClasses.map((x:any) => {
        return {
          replacedBy: this.courseClass.id,
          courseClass: x.id
        }
      });
      const items = event.data.courseClasses.map((x:any) => {
        return {
          replacedBy: this.courseClass.id,
          courseClass: x.id
        }
      });
      previous.forEach((x: any) => {
        if (items.findIndex((y: any) => y.courseClass === x.courseClass) === -1) {
          Object.assign(x, { $state: 4 });
          items.push(x);
        }
      });
      this.context.model('OneRosterReplaceClasses').save(items).then(() => {
        this.loadingService.hideLoading();
        this.replaces$.next(items.filter((x: any) => x.$state !== 4));
      }).catch((err) => {
        this.loadingService.hideLoading();
        this.errorService.showError(err);
      })
    }
  }

  onFormLoad() {
    setTimeout(() => {
    this.isLoading = false;
    }, 500);
  }


}