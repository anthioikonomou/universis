import { TableColumnConfiguration } from "@universis/ngx-tables";

export const STUDY_PROGRAM_COURSE_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    "name":"studyProgramCourse/course/displayCode",
    "property": "displayCode",
    "title":"Courses.DisplayCode",
    "hidden": true,
    "optional": true
  },
  {
    "name":"studyProgramCourse/course/name",
    "property": "name",
    "title":"Courses.name",
    "hidden": true,
    "optional": true
  },
  {
    "name":"studyProgramCourse/course/department/abbreviation",
    "property": "department",
    "title":"Courses.Department",
    "hidden": true,
    "optional": true
  },
  {
    "name":"specialization/name",
    "property": "specialty",
    "title":"Courses.Specialty",
    "hidden": true,
    "optional": true
  },
  {
    "name":"studyProgramCourse/course/courseArea/name",
    "property": "courseArea",
    "title":"Courses.courseArea",
    "hidden": true,
    "optional": true
  },
  {
    "name":"sortIndex",
    "property": "sortIndex",
    "title":"StudyPrograms.SortIndex",
    "hidden": true,
    "optional": true
  },
  {
    "name":"semester/name",
    "property": "courseSemester",
    "title":"Courses.semester",
    "hidden": true,
    "optional": true
  },
  {
    "name":"courseType/abbreviation",
    "property": "courseTypeName",
    "title":"Courses.CourseType.Label",
    "hidden": true,
    "optional": true
  },
  {
    "name":"units",
    "property": "units",
    "title":"Courses.units",
    "hidden": true,
    "optional": true
  },
  {
    "name":"ects",
    "property": "ects",
    "title":"Courses.ects",
    "hidden": true,
    "optional": true
  },
  {
    "name":"coefficient",
    "property": "coefficient",
    "title":"Courses.Coefficient",
    "hidden": true,
    "optional": true
  }
]