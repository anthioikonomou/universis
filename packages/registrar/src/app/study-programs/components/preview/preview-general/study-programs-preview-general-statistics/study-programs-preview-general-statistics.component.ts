import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ChartOptions } from 'chart.js';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-study-programs-preview-general-statistics',
  templateUrl: './study-programs-preview-general-statistics.component.html',
  styleUrls: ['./study-programs-preview-general-statistics.component.scss']
})
export class StudyProgramsPreviewGeneralStatisticsComponent implements OnInit, OnDestroy {

  constructor( private _activatedRoute: ActivatedRoute,
               private _context: AngularDataContext) { }

  public statistics: any;
  public studyProgramID: any;
  public isLoading = true;
  public totalStudents = 0;
  private subscription: Subscription;

  // Doughnut
  public doughnutChartLabels;
  public doughnutChartData;
  public doughnutChartType = 'doughnut';
  public doughnutOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: true,
      position: 'bottom'
    }
  };

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.statistics = await this._context.model('Students').select('count(id) as total', 'studyProgramSpecialty')
        .where('studyProgram').equal(this.studyProgramID)
        .and('studentStatus/alternateName').equal('active')
        .groupBy('studyProgram', 'studyProgramSpecialty')
        .expand('studyProgramSpecialty')
        .getItems();
      this.loadStatistics();
    });
  }

  loadStatistics() {

    this.isLoading = true;

    const data = this.statistics;
    this.totalStudents = data
      // map count
      .map(x => x.total)
      // calculate sum
      .reduce((a, b) => a + b, 0);

    const specialties = [];
    const numberOfStudents = [];

    data.forEach(element => {
      specialties.push(element.studyProgramSpecialty.abbreviation || element.studyProgramSpecialty.name);
      numberOfStudents.push(element.total);
    });

    this.doughnutChartLabels = specialties;
    this.doughnutChartData = [numberOfStudents];
    this.isLoading = false;
  }


  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
