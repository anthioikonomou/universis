import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendMessageToStudentComponent } from './send-message-to-student.component';

describe('SendMessageToStudentComponent', () => {
  let component: SendMessageToStudentComponent;
  let fixture: ComponentFixture<SendMessageToStudentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendMessageToStudentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendMessageToStudentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
