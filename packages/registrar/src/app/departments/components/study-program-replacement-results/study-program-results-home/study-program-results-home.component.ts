import { Component, OnInit, OnDestroy,  ViewEncapsulation } from '@angular/core';

declare var $: any;

@Component({
  selector: 'app-study-program-results-home',
  templateUrl: './study-program-results-home.component.html',
  encapsulation: ViewEncapsulation.None
})
export class StudyProgramResultsHomeComponent implements OnInit, OnDestroy {

  constructor() {
  }

  ngOnInit() {
  }


  ngOnDestroy() {
  }
}


