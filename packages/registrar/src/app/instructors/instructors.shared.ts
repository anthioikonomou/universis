import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {environment} from '../../environments/environment';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {InstructorsDashboardOverviewCurrentClassesComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-currentClasses/instructors-dashboard-overview-currentClasses.component';
import {InstructorsDashboardOverviewExamsComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-exams/instructors-dashboard-overview-exams.component';
import {InstructorsDashboardOverviewThesesComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-theses/instructors-dashboard-overview-theses.component';
import {InstructorsDashboardOverviewSendingGradesComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-sendingGrades/instructors-dashboard-overview-sendingGrades.component';
import {TooltipModule} from 'ngx-bootstrap';
import {InstructorsDashboardOverviewProfileComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-profile/instructors-dashboard-overview-profile.component';
import {InstructorsDashboardOverviewSupervisedComponent} from './components/instructors-dashboard/instructors-dashboard-overview/instructors-dashboard-overview-supervised/instructors-dashboard-overview-supervised.component';
import {MessagesModule} from '../messages/messages.module';
import {RouterModule} from '@angular/router';
import {
  InstructorDashboardThesesConfigurationResolver,
  InstructorDashboardThesesRequestsConfigurationResolver
} from './components/instructors-dashboard/instructors-dashboard-theses/instructors-dashboard-theses-config.resolver';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        HttpClientModule,
        TooltipModule.forRoot(),
        MessagesModule,
        RouterModule

    ],
  declarations: [
    InstructorsDashboardOverviewCurrentClassesComponent, InstructorsDashboardOverviewExamsComponent,
    InstructorsDashboardOverviewThesesComponent, InstructorsDashboardOverviewSendingGradesComponent,
    InstructorsDashboardOverviewProfileComponent, InstructorsDashboardOverviewSupervisedComponent
  ],
  exports: [
    InstructorsDashboardOverviewCurrentClassesComponent, InstructorsDashboardOverviewExamsComponent,
    InstructorsDashboardOverviewThesesComponent, InstructorsDashboardOverviewSendingGradesComponent,
    InstructorsDashboardOverviewProfileComponent, InstructorsDashboardOverviewSupervisedComponent
  ],
  providers: [
    InstructorsDashboardOverviewCurrentClassesComponent, InstructorsDashboardOverviewExamsComponent,
    InstructorsDashboardOverviewThesesComponent, InstructorsDashboardOverviewSendingGradesComponent,
    InstructorsDashboardOverviewProfileComponent, InstructorsDashboardOverviewSupervisedComponent,
    InstructorDashboardThesesConfigurationResolver,
    InstructorDashboardThesesRequestsConfigurationResolver
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class InstructorsSharedModule implements OnInit {

  constructor(private _translateService: TranslateService) {
    this.ngOnInit().catch(err => {
      console.error('An error occurred while loading instructors shared module');
      console.error(err);
    });
  }

  async ngOnInit() {
    environment.languages.forEach( language => {
      import(`./i18n/instructors.${language}.json`).then((translations) => {
        this._translateService.setTranslation(language, translations, true);
      });
    });
  }

}
