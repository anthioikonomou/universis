import { Component, DoCheck, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TableConfiguration } from '@universis/ngx-tables';
import { cloneDeep } from 'lodash';
import { LoadingService, ModalService, DIALOG_BUTTONS, ErrorService, ReferrerRouteService, TemplatePipe, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-instructors-root',
  templateUrl: './instructors-root.component.html',
  styleUrls: ['./instructors-root.component.scss']
})
export class InstructorsRootComponent implements OnInit, OnDestroy {
  public model: any;
  public instructorId: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;
  referrerSubscription: Subscription;
  paramSubscription: Subscription;
  referrerRoute: any = {
    queryParams: {},
    commands: []
  };

  constructor(private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _context: AngularDataContext,
    private _template: TemplatePipe,
    private _resolver: TableConfigurationResolver,
    public referrer: ReferrerRouteService, 
    private _translateService: TranslateService, 
    private _loadingService: LoadingService,
    private _modalService: ModalService,
    private _errorService: ErrorService,
    private _toastService: ToastService
    ) {
  }
  ngOnDestroy(): void {
    if (this.referrerSubscription) {
      this.referrerSubscription.unsubscribe();
    }
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe((params: { id?: string }) => {
      this.instructorId = params.id;
      this._context.model('Instructors')
        .where('id').equal(params.id)
        .expand('department')
        .getItem().then((data) => {
          this.model = data;
          this._resolver.get('Instructors').subscribe((config: TableConfiguration) => {

            // get referrer
            this.referrerSubscription = this.referrer.routeParams$.subscribe(result => {
              const pathConfiguration = config as { paths?: { name: string, alternateName: string }[] };
              let redirectCommands = [
                '/', 'instructors'
              ]
              if (pathConfiguration.paths && pathConfiguration.paths.length) {
                const addCommands = pathConfiguration.paths[0].alternateName.split('/');
                redirectCommands = [
                  '/', 'instructors'
                ].concat(addCommands);
              }
              this.referrerRoute.commands = redirectCommands;
            });

            if (config.columns && this.model) {
              // get actions from config file
              this.actions = config.columns.filter((x: any) => {
                return x.actions;
              })
                // map actions
                .map((x: any) => x.actions)
                // get list items
                .reduce((a: any, b: any) => b, 0);

              // filter actions with student permissions
              this.allowedActions = this.actions.filter(x => {
                if (x.role) {
                  if (x.role === 'action' || x.role === 'method') {
                    return x;
                  }
                }
              });

              this.edit = this.actions.find(x => {
                if (x.role === 'edit') {
                  x.href = this._template.transform(x.href, this.model);
                  return x;
                }
              });
              this.actions = this.allowedActions;
              this.actions.forEach(action => {
                if (action.href) {
                  action.href = this._template.transform(action.href, this.model);
                }
                if (action.invoke) {
                  action.click = () => {
                    if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
                  };
                }
              });
            }
          });
        });
    });

  }

  async deleteInsructor(){
    
    try {
      const instructor = this.instructorId;
      const translations: {
        Description: string;
        ModalMessage: string;
        ModalWarningCannotDelete: {
          CourseClasses: string;
          Exams: string;
          Students: string;
          Courses: string;
          LinkedUser: string;
          Thesis: string;
        };
        ToastMessage: string;
      }
        = this._translateService.instant('Instructors.DeleteInstructor');
      // in the context of the current user, try to find
      // if instructor has a linked user 
      this._loadingService.showLoading();
      const linkedUser = this.model && this.model.user;
      if (linkedUser) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.LinkedUser);
      }
      const hasCourseClasses = await this._context.model('CourseClassInstructors')
          .where('instructor/id')
          .equal(instructor)
          .select('count(id) as total')
          .getItem();
      if (hasCourseClasses && hasCourseClasses.total) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.CourseClasses);
      }
      // if instructor has exams
      const hasExams = await this._context.model('CourseExamInstructors')
          .where('instructor/id')
          .equal(instructor)
          .select('count(id) as total')
          .getItem();
      if (hasExams && hasExams.total) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Exams);
      }
      // if instructor has students
      const hasCourses = await this._context.model('Courses')
          .where('instructor')
          .equal(instructor)
          .select('count(id) as total')
          .getItem();
      if (hasCourses && hasCourses.total) {
        this._loadingService.hideLoading();
        // if yes, then the delete action cannot be initiated
        return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Courses);
      }
       // if instructor is student counselor has students
       const hasStudents= await this._context.model('StudentCounselors')
       .where('instructor/id')
       .equal(instructor)
       .select('count(id) as total')
       .getItem();
   if (hasStudents && hasStudents.total) {
     this._loadingService.hideLoading();
     // if yes, then the delete action cannot be initiated
     return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Students);
   }

    // if instructor has thesisRole
    let hasThesis = await this._context.model('ThesisRoles')
      .where('member')
      .equal(instructor)
      .select('count(id) as total')
      .getItem();
    if (hasThesis && hasThesis.total) {
      this._loadingService.hideLoading();
      // if yes, then the delete action cannot be initiated
      return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Thesis);
    }
    
    
    // if instructor has thesis
    hasThesis = await this._context.model('Theses')
      .where('instructor')
      .equal(instructor)
      .select('count(id) as total')
      .getItem();
    if (hasThesis && hasThesis.total) {
      this._loadingService.hideLoading();
      // if yes, then the delete action cannot be initiated
      return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Thesis);
    }
    
    // if instructor has StudentThesisResults
    hasThesis = await this._context.model('StudentThesisResults')
      .where('instructor')
      .equal(instructor)
      .select('count(id) as total')
      .getItem();
    if (hasThesis && hasThesis.total) {
      this._loadingService.hideLoading();
      // if yes, then the delete action cannot be initiated
      return this._modalService.showInfoDialog(translations.Description, translations.ModalWarningCannotDelete.Thesis);
    }

      this._loadingService.hideLoading();
      // initiate a delete instructor action
      // first, get confirmation
      const dialogResult = await this._modalService.showWarningDialog(translations.Description,
        translations.ModalMessage,
        DIALOG_BUTTONS.OkCancel);
      if (dialogResult !== 'ok') {
        return Promise.resolve();
      }
      this._loadingService.showLoading();
      // try to delete the instructor
      await this._context.model('Instructors').remove({id: instructor});

      this._loadingService.hideLoading();
      // and on success, show a toast message
      this._toastService.show(translations.Description, translations.ToastMessage);
      // and navigate to the programs list after a bit
      return setTimeout(() => {
        return this._router.navigate(['instructors']);
      }, 800);
    } catch (err) {
      // show error
      this._errorService.showError(err, {
        continueLink: '.'
      });
    } finally {
      // ensure loading is hidden
      this._loadingService.hideLoading();
    }
  
  }
}
