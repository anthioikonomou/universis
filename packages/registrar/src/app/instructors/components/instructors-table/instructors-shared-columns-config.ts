import { TableColumnConfiguration } from "@universis/ngx-tables";

export const INSTRUCTOR_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: "id",
    property: "id",
    title: "Instructors.SharedColumns.id",
    hidden: true,
    optional: true
  },
  {
    name: "familyName",
    property: "familyName",
    title: "Instructors.SharedColumns.familyName",
    hidden: true,
    optional: true
  },
  {
    name: "givenName",
    property: "givenName",
    title: "Instructors.SharedColumns.givenName",
    hidden: true,
    optional: true
  },
  {
    name: "title",
    property: "title",
    title: "Instructors.SharedColumns.title",
    hidden: true,
    optional: true
  },
  {
    name: "middleName",
    property: "middleName",
    title: "Instructors.SharedColumns.middleName",
    hidden: true,
    optional: true
  },
  {
    name: "gender/name",
    property: "gender",
    title: "Instructors.SharedColumns.gender",
    hidden: true,
    optional: true
  },
  {
    name: "status/name",
    property: "status",
    title: "Instructors.SharedColumns.status",
    hidden: true,
    optional: true
  },
  {
    name: "category",
    property: "category",
    title: "Instructors.SharedColumns.category",
    hidden: true,
    optional: true
  },
  {
    name: "isTeaching",
    property: "isTeaching",
    title: "Instructors.SharedColumns.isTeaching",
    formatter: "TrueFalseFormatter",
    hidden: true,
    optional: true
  },
  {
    name: "specialty",
    property: "specialty",
    title: "Instructors.SharedColumns.specialty",
    hidden: true,
    optional: true
  },
  {
    name: "department/name",
    property: "department",
    title: "Instructors.SharedColumns.department",
    hidden: true,
    optional: true
  },
  {
    name: "homeCity",
    property: "homeCity",
    title: "Instructors.SharedColumns.homeCity",
    hidden: true,
    optional: true
  },
  {
    name: "country/alpha2code",
    property: "country",
    title: "Instructors.SharedColumns.country",
    hidden: true,
    optional: true
  },
  {
    name: "homePhone",
    property: "homePhone",
    title: "Instructors.SharedColumns.homePhone",
    hidden: true,
    optional: true
  },
  {
    name: "workCity",
    property: "workCity",
    title: "Instructors.SharedColumns.workCity",
    hidden: true,
    optional: true
  },
  {
    name: "email",
    property: "email",
    title: "Instructors.SharedColumns.email",
    hidden: true,
    optional: true
  },
  {
    name: "workPhone",
    property: "workPhone",
    title: "Instructors.SharedColumns.workPhone",
    hidden: true,
    optional: true
  },
  {
    name: "url",
    property: "url",
    title: "Instructors.SharedColumns.url",
    hidden: true,
    optional: true
  },
];