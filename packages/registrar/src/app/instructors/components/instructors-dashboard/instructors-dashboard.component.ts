import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-instructors-dashboard',
  templateUrl: './instructors-dashboard.component.html',
  styleUrls: ['./instructors-dashboard.component.scss']
})
export class InstructorsDashboardComponent implements OnInit {
  public model: any;
  public tabs: any[];

constructor(private _activatedRoute: ActivatedRoute,
  private _context: AngularDataContext) {
}

async ngOnInit() {
  this.tabs = this._activatedRoute.routeConfig.children.filter( route => typeof route.redirectTo === 'undefined' );

  this.model = await this._context.model('Instructors')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .expand('department($expand=organization),user')
    .getItem();
}
}
