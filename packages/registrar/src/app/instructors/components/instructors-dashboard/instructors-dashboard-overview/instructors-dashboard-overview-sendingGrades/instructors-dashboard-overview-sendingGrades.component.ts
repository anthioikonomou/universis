import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-instructors-dashboard-overview-sendingGrades',
  templateUrl: './instructors-dashboard-overview-sendingGrades.component.html',
  styleUrls: ['../../instructors-dashboard.component.scss']
})
export class InstructorsDashboardOverviewSendingGradesComponent implements OnInit {
  @Input() id: any;
  @Input() gender: any;
  public model: any;
  public recentGrading: any = [];
  public grading_arr: any;
  public index_arr: any;


  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('ExamDocumentUploadActions')
      .asQueryable()
      .expand('object')
      .where('owner').equal(20018003)
      .where('actionStatus/alternateName').equal('CompletedActionStatus')
      .orderByDescending('dateModified')
      .getItems();
    this.index_arr = removeDuplicates(this.model);
    this.recentGrading.length = 0;
    for (let i = 0; i < this.index_arr.length; i++) {
      this.grading_arr = this.model.find(x =>
        x.object.id === this.index_arr[i]).object;
      this.recentGrading.push(this.grading_arr);
    }
  }
}

function removeDuplicates(arr) {
  const unique_array = [];
  const unique_index_array = [];
  for (let i = 0; i < arr.length; i++) {
    if (unique_index_array.indexOf(arr[i].object.id) === -1) {
      unique_index_array.push(arr[i].object.id);
    }
  }
  return unique_index_array;
}



