import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { AppEventService } from '@universis/common';
import { QueryBuilderComponent, QueryBuilderService } from '@universis/ngx-query-builder';
import { ActivatedTableService, AdvancedTableComponent } from '@universis/ngx-tables';
import { BsModalRef, BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { MD5 } from 'crypto-js';
import { TranslateService } from '@ngx-translate/core';
import { UserQuery } from './model';
import { ActivatedRoute, Router } from '@angular/router';
import { from, Observable } from 'rxjs';
import { UserQueryHistoryComponent } from './components/user-query-history.component';
import { ApplicationDatabase } from '../registrar-shared/services/app-db.service';
@Injectable()
export class ActiveTableQueryBuilder {

  private modalRef: BsModalRef;
  private historModalRef: BsModalRef;
  private target: AdvancedTableComponent;
  public query: any = {
    type: 'LogicalExpression',
    operator: '$and',
    elements: [
      {
        type: 'BinaryExpression',
        operator: '$eq',
        left: null,
        right: null
      }
    ]
  };

  constructor(private context: AngularDataContext,
    private activatedTable: ActivatedTableService,
    private modalService: BsModalService,
    private appEvent: AppEventService,
    private translateService: TranslateService,
    private queryBuilderService: QueryBuilderService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private applicationDatabase: ApplicationDatabase) {
  }

  open(builderTemplate: any) {
    const initialState: ModalOptions = {
      class: 'modal-xl query-builder-container',
      initialState: {
      }
    };
    this.target = this.activatedTable.activeTable;
    this.modalRef = this.modalService.show(builderTemplate, initialState);
  }

  close() {
    if (this.modalRef) {
      this.modalRef.hide();
    }
  }

  find(queryParams: any): Observable<UserQuery> {
    const hashCode = MD5(JSON.stringify(queryParams)).toString();
    return from(this.applicationDatabase.db.table<UserQuery>('UserQuery').where('hashCode').equals(hashCode).first());
  }

  getHistory(forEntityType: string): Observable<UserQuery[]> {
    return from(this.applicationDatabase.db.table<UserQuery>('UserQuery').where('entityType').equals(forEntityType).toArray());
  }

  navigateWith(builder: QueryBuilderComponent) {
    this.modalRef.hide();
    const target = this.activatedTable.activeTable;
    if (target != null) {
      const queryParams = builder.queryParams;

      const item: UserQuery = {
        query: builder.query,
        hashCode: MD5(JSON.stringify(queryParams)).toString(),
        entityType: builder.entityType,
        description: builder.queryDescription,
        dateCreated: new Date(),
        dateAccessed: new Date()
      }
      void this.applicationDatabase.db.table<UserQuery>('UserQuery').filter((x) => x.hashCode === item.hashCode).first().then((result) => {
        if (result) {
          return this.navigateWithQuery(result);
        }
        this.applicationDatabase.db.table<UserQuery>('UserQuery').put(item);
        if (target.query) {
          target.query.setParam('$filter', null);
        }
        this.router.navigate([], {
          queryParams: {
            $filter: queryParams.$filter
          },
          relativeTo: this.activatedRoute,
          queryParamsHandling: 'merge'
        }).then(() => {
          target.fetch(true);
          this.activatedTable.activeTable = target;
          this.appEvent.change.next({
            model: 'UserQuery',
            target: item
          });
        });
      });
    }
  }

  clear() {
    const target = this.activatedTable.activeTable;
    if (target != null) {
      if (target.query) {
        target.query.setParam('$filter', null);
      }
      this.router.navigate([], {
        queryParams: {
          $filter: ''
        },
        relativeTo: this.activatedRoute
      }).then(() => {
        target.fetch(true);
        this.activatedTable.activeTable = target;
      });
    }
  }

  navigateWithQuery(item: UserQuery) {
    item.dateAccessed = new Date();
    const target = this.activatedTable.activeTable;
    setTimeout(() => {
      const builder = new QueryBuilderComponent(this.context, this.translateService, this.queryBuilderService);
      builder.query = item.query;
      builder.entityType = item.entityType;
      const queryParams = builder.queryParams;

      item.dateAccessed = new Date();
      this.applicationDatabase.db.table<UserQuery>('UserQuery').update(item.id, item);
      if (target.query) {
        target.query.setParam('$filter', null);
      }
      this.router.navigate([], {
        queryParams: {
          $filter: queryParams.$filter
        },
        relativeTo: this.activatedRoute,
        queryParamsHandling: 'merge'
      }).then(() => {
        target.fetch(true);
        this.activatedTable.activeTable = target;
        this.appEvent.change.next({
          model: 'UserQuery',
          target: item
        });
      });
    });
  }

  showHistory(forEntityType: string) {
    this.historModalRef = this.modalService.show(UserQueryHistoryComponent, {
      backdrop: true,
      class: 'modal-xl',
      initialState: {
        entityType: forEntityType
      }
    });
    const component:UserQueryHistoryComponent =  this.historModalRef.content;
    component.closing.subscribe(() => {
      this.historModalRef.hide();
    });
    component.navigate.subscribe((event: {
      target: UserQueryHistoryComponent,
      item: UserQuery
    }) => {
      this.navigateWithQuery(event.item);
      setTimeout(() => {
        this.historModalRef.hide();
      }, 250);
    });
  }

}